# coding:utf-8
# 如何返回HTML文件？尝试修改一下文件内容刷新页面看看是否有变化？

from flask import Flask, render_template
app = Flask(__name__)


@app.route("/")
def hello():
    return render_template("hello.html")


app.run()
