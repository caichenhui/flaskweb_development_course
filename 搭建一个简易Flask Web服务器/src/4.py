# coding:utf-8
# 完成前后端完整交互

from flask import Flask, render_template, request
app = Flask(__name__)


@app.route("/")
def hello():
    return render_template("interface.html")


# 接收两个整数num1, num2，返回其相加后的结果
@app.route('/add', methods=['POST'])
def add():
    num1 = request.form.get('num1')
    num2 = request.form.get('num2')

    return '{"status":0, "result":%d}' % (int(num1) + int(num2))


app.run()
