# coding:utf-8
# GET、POST接口设计

from flask import Flask, render_template
app = Flask(__name__)


@app.route("/")
def hello():
    return render_template("hello.html")


@app.route('/test-get', methods=['GET'])
def test_get():
    return "It's a GET method!"


@app.route('/test-post', methods=['POST'])
def test_post():
    return "It's a POST method!"


app.run()
